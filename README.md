# 邵阳市第一中学信息技术社团

#### 介绍
[社团](https://sysyz-itcom.github.io)于2024年1月6日由邵阳市第一中学在校学生创立，旨在宣传计算机技术和聚集志同道合的人互相交流、切磋，解决问题、传授知识，在一个其乐融融的氛围中提升自己的技术能力并扩宽自身的眼界。

#### 加入我们
你可以在以下位置关注或联系我们
- [官方BiliBili账号](https://b23.tv/yFKX8Fy)
- [官方GitHub账号](https://github.com/Sysyz-itcom-club)
- [官方博客](https://itcom6.wordpress.com)
- 邮箱：sysyzitcom@163.com
