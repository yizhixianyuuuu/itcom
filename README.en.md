# Shaoyang No. 1 Middle School Information Technology Club

#### introduce
Founded on January 6, 2024 by the students of Shaoyang No. 1 Middle School, the club aims to promote computer technology and gather like-minded people to communicate with each other, learn from each other, solve problems, impart knowledge, and improve their technical skills and broaden their horizons in a happy atmosphere.

#### Directions

1.  This repository is only used by club members to upload files.
2.  If you want to submit your code, please read the [Declaration Document](https://github.com/Sysyz-itcom/itcom/wiki/%E9%A6%96%E9%A1%B5) first.。

